﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterDoor : MonoBehaviour
{
    
    public SpriteRenderer spriteRenderer;
    public Sprite status1;
    public Sprite status2;
    // Start is called before the first frame update
    void Start()
    {
        Key.keys = 0;
    }

    void OnMouseDown()
    {
        if (gameObject.CompareTag("Pedestal") && Key.keys == 1)
        {
            spriteRenderer.sprite = status1;
        }
        else if (gameObject.CompareTag("Pedestal") && Key.keys == 2)
        {
            spriteRenderer.sprite = status2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
