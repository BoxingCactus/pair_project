﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigDoor1 : MonoBehaviour
{

    public AudioSource playSound;
    public Vector2 Onscreen;
    public Vector2 Offscreen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (BigPressure1.BPlayerOn1 == true)
            {
                transform.position = Offscreen;
                playSound.Play();
            } 
        if (BigPressure1.BPlayerOn1 != true)
            {
                transform.position = Onscreen;
            } 
    }
}
