﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallDoor2 : MonoBehaviour
{

    public AudioSource playSound;
    public AudioSource lockSound;
    public Vector2 Onscreen;
    public Vector2 Offscreen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SmallPressure2.SPlayerOn2 == true)
            {
                transform.position = Offscreen;
                playSound.Play();
            } 
        if (SmallPressure2.SPlayerOn2 != true)
            {
                transform.position = Onscreen;
            } 
    }
}
