﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallPressure1 : MonoBehaviour
{
    public AudioSource playSound;
    public static bool SPlayerOn1;

    public bool SSound = false;

    void Start()
    {
        SPlayerOn1 = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure1") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn1 = true;
            SSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure1") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn1 = false;
            SSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SPlayerOn1 == true)
        {
            if (SSound == false)
            {
                playSound.Play();
                SSound = true;
            }

        }
   
    }
}
