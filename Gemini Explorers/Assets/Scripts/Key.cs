﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public static int keys;
    // Start is called before the first frame update
    void Start()
    {
        keys = 0;
    }
    void OnMouseDown()
    {
        if (gameObject.CompareTag("Key"))
        {
            keys += 1;
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
