﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPressure1 : MonoBehaviour
{
    public AudioSource playSound;
    public static bool BPlayerOn1;

    public bool BSound = false;

    void Start()
    {
        BPlayerOn1 = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure1") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn1 = true;
            BSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure1") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn1 = false;
            BSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (BPlayerOn1 == true)
        {
            if (BSound == false)
            {
                playSound.Play();
                BSound = true;
            }
        }
    }
}
