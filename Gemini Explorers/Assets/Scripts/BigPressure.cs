﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPressure : MonoBehaviour
{
    public AudioSource playSound;
    public static bool BPlayerOn;

    public bool BSound = false;

    void Start()
    {
        BPlayerOn = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn = true;
            BSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn = false;
            BSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (BPlayerOn == true)
        {
            if (BSound == false)
            {
                playSound.Play();
                BSound = true;
            }
        }
    }
}
