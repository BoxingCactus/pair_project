﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
      Rigidbody2D rb = GetComponent<Rigidbody2D>();
      if (Input.GetKey(KeyCode.A))
            rb.transform.Translate(Vector2.left * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            rb.transform.Translate(Vector2.right * speed * Time.deltaTime);

      if (Input.GetKey(KeyCode.W))
            rb.transform.Translate(Vector2.up * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            rb.transform.Translate(Vector2.down * speed * Time.deltaTime);
    if (Input.GetKey(KeyCode.R))
            SceneManager.LoadScene("Level3");
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Stairs")) 
        {
            SceneManager.LoadScene("Level3");
        }
    }
}
