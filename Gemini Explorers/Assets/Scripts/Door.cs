﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    public AudioSource playSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (Pressure.BPlayerOn == true && Pressure.SPlayerOn)
        {
            gameObject.SetActive(false);
            playSound.Play();
        } 
    }
}
