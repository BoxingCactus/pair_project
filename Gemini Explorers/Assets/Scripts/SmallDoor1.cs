﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallDoor : MonoBehaviour
{

    public AudioSource playSound;
    public AudioSource lockSound;
    public Vector2 Onscreen;
    public Vector2 Offscreen;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SmallPressure.SPlayerOn == true)
            {
                transform.position = Offscreen;
                playSound.Play();
            } 
        if (SmallPressure.SPlayerOn != true)
            {
                transform.position = Onscreen;
            } 
    }
}
