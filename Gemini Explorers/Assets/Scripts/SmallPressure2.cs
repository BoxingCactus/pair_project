﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallPressure2 : MonoBehaviour
{
    public AudioSource playSound;
    public static bool SPlayerOn2;

    public bool SSound = false;

    void Start()
    {
        SPlayerOn2 = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure2") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn2 = true;
            SSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure2") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn2 = false;
            SSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SPlayerOn2 == true)
        {
            if (SSound == false)
            {
                playSound.Play();
                SSound = true;
            }

        }
   
    }
}
