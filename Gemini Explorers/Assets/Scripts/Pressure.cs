﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pressure : MonoBehaviour
{
    public AudioSource playSound;
    public static bool BPlayerOn;
    public static bool SPlayerOn;

    public bool SSound = false;
    public bool BSound = false;

    void Start()
    {
        BPlayerOn = false;
        SPlayerOn = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn = true;
            BSound = true;

        }
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn = true;
            SSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("BigPressure") && collision.gameObject.tag == "BigPlayer")
        {
            BPlayerOn = false;
            BSound = false;
        }
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn = false;
            SSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (BPlayerOn == true)
        {
            if (BSound == false)
            {
                playSound.Play();
                BSound = true;
            }
        }
        if (SPlayerOn == true)
        {
            if (SSound == false)
            {
                playSound.Play();
                SSound = true;
            }

        }
   
    }
}
