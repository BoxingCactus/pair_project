﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallDestroyPressure : MonoBehaviour
{
    public AudioSource playSound;
    public static bool SPlayerDestroy;

    public bool SSound = false;

    void Start()
    {
        SPlayerDestroy = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerDestroy = true;
            SSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerDestroy = false;
            SSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SPlayerDestroy == true)
        {
            if (SSound == false)
            {
                playSound.Play();
                SSound = true;
            }

        }
   
    }
}
