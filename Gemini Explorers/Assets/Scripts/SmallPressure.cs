﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallPressure : MonoBehaviour
{
    public AudioSource playSound;
    public static bool SPlayerOn;

    public bool SSound = false;

    void Start()
    {
        SPlayerOn = false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn = true;
            SSound = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.CompareTag("SmallPressure") && collision.gameObject.tag == "SmallPlayer")
        {
            SPlayerOn = false;
            SSound = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SPlayerOn == true)
        {
            if (SSound == false)
            {
                playSound.Play();
                SSound = true;
            }

        }
   
    }
}
