﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableDoor : MonoBehaviour
{

    public AudioSource playSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (SmallDestroyPressure.SPlayerDestroy == true)
        {
            gameObject.SetActive(false);
            playSound.Play();
        } 
    }
}
